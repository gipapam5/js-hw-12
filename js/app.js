const buttons = document.querySelectorAll('.btn');

document.addEventListener('keydown', (event) => {
  const pressedKey = event.key;
    const buttonToColor = Array.from(buttons).find((button) => {
        return button.textContent === pressedKey;
    });
    
    buttons.forEach((button) => {
        button.style.backgroundColor = '';
        if (buttonToColor) {
            buttonToColor.style.backgroundColor = 'blue';
        } else if (!buttonToColor) {
            button.style.backgroundColor = '';
        }
    })
});