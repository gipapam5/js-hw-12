1.  Визначити яку клавішу натиснув користувач, можна за допомогою події keydown або keyup. Виглядати це може наступним чином:
    window.addEventListener('keydown' (event) => {
    console.log(event.key);
    })
2.  Різниця між event.code() та event.key() полягає у тому, що event.key() - покаже нам саме букву яку натисне користувач в залежності від розкладки, а event.code() - покаже нам клавіатурну кнопку яку натискає користувач. Наприклад:

        window.addEventListener('keydown', (e) => {
        console.log(e.key);
        console.log(e.code);

    })

    У цьому коді якщо користувач натисне Z на укр. розкладці то event.key() - покаже нам 'я', а event.code() 'KeyZ'

3.  Існує 3 події клавіатури - це 'keydown', 'keyup' і 'keypress'. Різниця у тому коли спрацює кожна подія. Наприклад keydown спрацює як тільки користувач натисне кнопку, keyup - коли він цю кнопку відпустить, а keypress - це цілий процес натиску і відпускання кнопки ( повне натискання кнопки)
